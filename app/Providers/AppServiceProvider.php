<?php

namespace CodeAgenda\Providers;

use CodeAgenda\Entities\Pessoa;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    public function boot() {
        view()->share(['letras' => $this->getLetras(), 'search' => '', 'errors' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * @return array
     */
    protected function getLetras() {
        return Pessoa::selectRaw('UPPER(SUBSTRING(apelido, 1, 1)) AS letra')->groupBy('letra')->orderBy('letra')->get();
    }
}
