<?php

namespace CodeAgenda\Entities;


use Illuminate\Database\Eloquent\Model;

class Telefone extends Model {

    protected $table = 'telefones';
    protected $fillable = [
        'descricao',
        'codPais',
        'ddd',
        'prefixo',
        'sufixo'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pessoa(){
        return $this->belongsTo(Pessoa::class);
    }

    /**
     * @return string
     */
    public function getNumeroAttribute() {
        return "{$this->codPais} ({$this->ddd}) {$this->prefixo}-{$this->sufixo}";
    }
}