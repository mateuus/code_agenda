<?php

namespace CodeAgenda\Http\Controllers;

use CodeAgenda\Entities\Pessoa,
    Illuminate\Support\Facades\Input;

class AgendaController extends Controller {

    public function index($letra = 'A') {
        $pessoas = [];
        $search = '';
        if (strlen(Input::get('search')) > 0) {
            $search = Input::get('search');
            $pessoas = Pessoa::where('nome', 'LIKE', "%{$search}%")->orWhere('apelido', 'LIKE', "%{$search}%")->get();
        } else {
            $pessoas = Pessoa::where('apelido', 'LIKE', "{$letra}%")->get();
        }

        return view('agenda', compact('pessoas', 'search', 'letras'));
    }

}