<?php

namespace CodeAgenda\Http\Controllers;

use CodeAgenda\Entities\Telefone;

class TelefoneController extends Controller {

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) {

        Telefone::destroy($id);


        return redirect()->route('agenda.index');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function delete($id) {
        $telefone = Telefone::find($id);
        $pessoa = $telefone->pessoa;
        return view('telefone.delete', compact('telefone', 'pessoa'));
    }
}