<?php

namespace CodeAgenda\Http\Controllers;

use CodeAgenda\Entities\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PessoaController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('pessoa.create');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|min:3|max:250|unique:pessoas',
            'apelido' => 'required|min:2|max:50',
            'sexo' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $inputs = $request->input();
            return view('pessoa.create', compact('errors', 'inputs'));
        }
        Pessoa::create($request->all());

        return redirect()->route('agenda.index');
    }

    public function update($id) {

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) {

        Pessoa::destroy($id);

        return redirect()->route('agenda.index');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function delete($id) {
        $pessoa = Pessoa::find($id);
        return view('pessoa.delete', compact('pessoa'));
    }
}