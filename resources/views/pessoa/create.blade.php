@extends('layout')
@section('content')
    <div class="col-md-6">
        <form action="{{ route('pessoa.store') }}" method="POST">
            <div class="form-group row">
                <label for="nome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome completo">
                </div>
            </div>
            <div class="form-group row">
                <label for="apelido" class="col-sm-2 col-form-label">Apelido</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="apelido" name="apelido" placeholder="Apelido">
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Sexo</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="sexo" id="sexo1"
                                   value="M" checked>
                            <label class="form-check-label" for="sexo1">
                                <i class="fa fa-male"></i>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="sexo" id="sexo2"
                                   value="F">
                            <label class="form-check-label" for="sexo2">
                                <i class="fa fa-female"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection