<div class="panel {{ $pessoa->sexo == 'M' ? 'panel-info' : 'panel-danger' }}">
    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="pull-left">
                <span><i class="fa {{ $pessoa->sexo == 'M' ? 'fa-male' : 'fa-female' }}"></i></span>
                <span>{{ $pessoa->apelido }}</span>
            </div>
            <div class="pull-right">
                <a href="#" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                <a href="{{route('pessoa.delete', ['idPessoa' => $pessoa->id])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Apagar"><i class="fa fa-remove"></i>
                </a>
            </div>
        <h3>
    </div>
    <div class="panel-body">
        <h3>{{ $pessoa->nome }}</h3>
        @include('partials.telefones', ['telefones' => $pessoa->telefones])
    </div>
</div>