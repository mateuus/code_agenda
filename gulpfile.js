var elixir = require('laravel-elixir');
var bowerDir = 'vendor/bower_components/';

elixir(function (mix) {
   mix.copy(bowerDir + 'bootstrap-less/fonts', 'public/fonts')
   .copy(bowerDir + 'font-awesome-less/fonts', 'public/fonts')
       .copy(bowerDir + 'jquery/dist/jquery.min.js', 'resources/assets/js')
       .copy(bowerDir + 'bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js')
       .scripts([
           'jquery.min.js',
           'bootstrap.min.js',
           'custom.js'
       ], 'public/js/scripts.js')
       .less('app.less');
});