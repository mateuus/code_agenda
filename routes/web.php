<?php

$router->get('/', ['as' => 'agenda.index', 'uses' => 'AgendaController@index']);
$router->get('/{letra}', ['as' => 'agenda.letra', 'uses' => 'AgendaController@index']);

$router->get('/pessoas/novo', ['as' => 'pessoa.create', 'uses' => 'PessoaController@create']);
$router->post('/pessoas', ['as' => 'pessoa.store', 'uses' => 'PessoaController@store']);
$router->get('/pessoas/{idPessoa}/editar', ['as' => 'pessoa.update', 'uses' => 'PessoaController@update']);
$router->get('/pessoas/{idPessoa}/apagar', ['as' => 'pessoa.delete', 'uses' => 'PessoaController@delete']);
$router->delete('/pessoas/{idPessoa}', ['as' => 'pessoa.destroy', 'uses' => 'PessoaController@destroy']);

$router->get('telefones/{idTelefone}/apagar', ['as' => 'telefone.delete', 'uses' => 'TelefoneController@delete']);
$router->delete('telefones/{idTelefone}', ['as' => 'telefone.destroy', 'uses' => 'TelefoneController@destroy']);
